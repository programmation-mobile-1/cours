---
layout: layouts/page.njk
date: Last Modified
title: JSON
---

Déclarer un structure

```
sNomFichier est une chaîne = fConstruitChemin(SysRep(srAppDataCommun)+"\NEOTECH",gsBDDSage,".json")
sContenuFichier	est une chaîne = UTF8VersUnicode(fChargeTexte(sNomFichier))

SI (sContenuFichier <> "") ALORS

  QUAND EXCEPTION DANS
  Désérialise(gParametres, sContenuFichier, psdJSON)
FIN
```
