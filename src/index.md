---
layout: layouts/page.njk
date: Last Modified
---

Programme de la licence professionnelle [AMIO](amio)

## Prérequis
+ Utiliser Docker

## Installation des Serveurs

### Serveur de base de données HFSQL

```shell-session
docker pull windev/webdev:FR260101
```

```shell-session
docker run -dit ^
--restart unless-stopped ^
--name hfsql-iutsd ^
--volume hfsql-iutsd:/var/lib/hfsql ^
--publish 9009:4900 ^
--net iutsd-net ^
--ip 192.168.0.109 ^
--dns 172.16.6.177 ^
-e HFSQL_PASSWORD=xgL43wAwGZLUJdko5LOb ^
windev/hfsql:FR260101
```

### Serveur d'applications Web WebDev

```shell-session
docker pull windev/webdev:FR260101d
```

```shell-session
docker run -dit ^
--restart unless-stopped ^
--name webdev-iutsd ^
--volume webdev-iutsd:/var/lib/WEBDEV/26.0/ ^
--publish 9008:80 ^
--net iutsd-net ^
--ip 192.168.0.108 ^
--dns 172.16.6.177 ^
windev/webdev:FR260101d
```

## Exercice : Application de présence

Dans un analyse ajouter 2 tables pour gérer des événement et des personnes inscrites à ces événement.

Ajouter les personnes, Utiliser le champ code barre pour générer un qcode pour la personnes

Stocker dans un fichier JSON les informations de connexion au serveur HFSQL (Docker)

Lors de l'ouverture du programme se connecter au serveur HFSQL

Créer un webservice pour exposer la table des personnes

Créer une application mobile pour lire le qrcode



## Analyse

[crud](crud)

## Web Serice

[json](json)

### Web Service SOAP
[SOAP](soap)

### Web Service REST
