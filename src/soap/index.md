---
layout: layouts/page.njk
date: Last Modified
title: SOAP
---


SOAP (Simple Object Access Protocol) est un protocole de transmission de messages. Il définit un ensemble de règles pour structurer des messages qui peuvent être utilisés dans de simples transmissions unidirectionnelles, mais il est particulièrement utile pour exécuter des dialogues requête-réponse ou appels de procédures à distances RPC (Remote Procedure Call). Il n'est pas lié à un protocole de transport particulier mais HTTP est le plus populaire. Il peut utiliser SMTP ou POP ou d’autres protocoles.

Il n'est pas non plus lié à un système d'exploitation ni à un langage de programmation, donc, théoriquement, les clients et serveurs de ces dialogues peuvent tourner sur n'importe quelle plate-forme et être écrits dans n'importe quel langage du moment qu'ils puissent formuler et comprendre des messages SOAP. Il s'agit donc d'un important composant de base pour développer des applications distribuées qui exploitent des fonctionnalités publiées comme des services par des intranets ou internet (services web).

Les paquets de données circulent sous forme de texte structuré au format **XML** (Extensible Markup Language).

SOAP a été initialement défini par Microsoft et IBM, mais est devenu une référence depuis une recommandation du W3C, utilisée notamment dans le cadre d'architectures de type SOA (Service Oriented Architecture) pour les Services Web WS-*. Il est basé sur le protocole XML-RPC qui est considéré comme son ancêtre et est d’ailleurs plus beaucoup utilisé.

Le protocole SOAP est composé de deux parties :

  une enveloppe, contenant des informations sur le message lui-même afin de permettre son acheminement et son traitement,
  un modèle de données, définissant le format du message, c'est-à-dire les informations à transmettre.


## Message SOAP

Un message SOAP est encodé en XML, il est constitué d’une enveloppe qui contient elle-même un élément header optionnel, un élément body. Un élément fault peut-être contenu dans l’élément body pour spécifier les erreurs.

```xml
<?xml version='1.0' ?>
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
  <m:reservation xmlns:m="http://travelcompany.example.org/reservation"
      env:role="http://www.w3.org/2003/05/soap-envelope/role/next"
      env:mustUnderstand="true">
    <m:reference>uuid:093a2da1-q345-739r-ba5d-pqff98fe8j7d</m:reference>
    <m:dateAndTime>2021-11-29T13:20:00.000-05:00</m:dateAndTime>
  </m:reservation>
  <n:passenger xmlns:n="http://mycompany.example.com/employees"
      env:role="http://www.w3.org/2003/05/soap-envelope/role/next"
      env:mustUnderstand="true">
   <n:name>Albert Einstein</n:name>
  </n:passenger>
</env:Header>
<env:Body>
  <p:itinerary xmlns:p="http://travelcompany.example.org/reservation/travel">
   <p:departure>
     <p:departing>New York</p:departing>
     <p:arriving>Los Angeles</p:arriving>
     <p:departureDate>2022-02-14</p:departureDate>
     <p:departureTime>late afternoon</p:departureTime>
     <p:seatPreference>aisle</p:seatPreference>
   </p:departure>
   <p:return>
     <p:departing>Los Angeles</p:departing>
     <p:arriving>New York</p:arriving>
     <p:departureDate>2022-02-20</p:departureDate>
     <p:departureTime>mid-morning</p:departureTime>
     <p:seatPreference/>
   </p:return>
  </p:itinerary>
  <q:lodging xmlns:q="http://travelcompany.example.org/reservation/hotels">
   <q:preference>none</q:preference>
  </q:lodging>
</env:Body>
</env:Envelope>
```

Les services SOAP sont très bien intégrés à l’environnement Microsoft Visual Studio, ils fonctionnent parfaitement dans un environnement .NET comme un serveur ASP .net et des applications C#

## WSDL

C'est une description fondée sur le XML qui indique « comment communiquer pour utiliser le service »

Le fichier WSDL liste les types de données et les méthodes contenues dans le web service. Le WSDL renseigne sur la localisation du service (URL)

Le WSDL décrit une Interface publique d'accès à un Service Web, notamment dans le cadre d'architectures de type SOA (Service Oriented Architecture).

C'est une description fondée sur le XML qui indique « comment communiquer pour utiliser le service »;

Le WSDL sert à décrire :

-	le protocole de communication (SOAP RPC ou SOAP orienté message)
-	le format de messages requis pour communiquer avec ce service
-	les méthodes et les paramètres que le client peut invoquer
-	la localisation du service.
