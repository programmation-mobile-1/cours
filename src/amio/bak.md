

## Programme pédagogique

Volume horaire global par étudiant de la formation (hors projet tutoré et hors stage)


450 h

Volume horaire du projet tutoré


150 h

Durée du stage 12 à 16 semaines



## SEMESTRE 1 : 30 crédits européens



UE 501


C


Compétences attendues


## Interfaces et documents web


**ECTS :** 8

Conception et réalisation d'interfaces et de documents pour le web, basés sur les technologies standardisées du W3C : html5, css3, dom, javascript, conception adaptative, conception de l'interaction, éléments d'ergonomie. Un module optionnel est consacré à des contenus de remise à niveau pour les étudiants n'atteignant pas l'ensemble des prérequis dans le domaine du développement logiciel.


90

### 501.1 : Programmation web côté client

Javascript / dom / widget & frameworks ui

**ECTS :** 2

**Nombre d'heures :** 25

**Enseignant :**



### 501.3.1 : Conception pour le web


**ECTS :** 2


Besoins / cas d'utilisation / UX design / uml
Enseignant :


25

### 501.3.2 : Techniques de développement


Remise à niveau programmation/ingénierie
Enseignant :



UE 502


C


Compétences attendues


H

Environnement professionnel


**ECTS :** 8


90

### 502.1 : Anglais

Anglais professionnel et technique

**ECTS :** 2

**Nombre d'heures :** 25 h

**Enseignant :** Rebecca Companie

### 502.2 : Droit de l’internet

Droit des TICs et de l'internet, droit à l'image, propriété intellectuelle et droits d'auteurs, licences logicielles.

**ECTS :** 2

**Nombre d'heures :** 20 h

**Enseignant :**

### 502.3 : Communication professionnelle

Communication écrite et orale en situation professionnelle - présentations et rapports techniques

**ECTS :** 2

**Nombre d'heures :** 20h

**Enseignant :** Anna Ricci

### 502.4 : Gestion de projets web

Méthodes et outils pour la gestion de projets agiles et/ou traditionnels

**ECTS :** 2

**Nombre d'heures :** 25h

**Enseignant :**

UE 503


C


Compétences attendues


H

Développement / administration serveur


**ECTS :** 8

**Nombre d'heures :** 90

### 503.1 : Programmation web sur le serveur

Programmation sur le serveur web : architecture et patterns (MVC), accès aux données, gestion des sessions, authentification et contrôle d'accès, sécurité, accès à un serveur tiers

**ECTS :** 4

**Nombre d'heures :** 40h

**Enseignant :**

### 503.2 : Gestion de données pour la mobilité

Gestion par flux et par lots des données, mise en œuvre de conteneurs de données

**ECTS :** 2

**Nombre d'heures :** 20h

**Enseignant :**

### 503.2 : Sécurité réseau & cloud

Les objets tiers, types et architectures des plateformes IoT, mise en œuvre

**ECTS :** 2

**Nombre d'heures :** 30h


UE 504


C


Compétences attendues


H

Compétences de spécialisation


**ECTS :** 6


70

### 504.1 : Objets connectés

Caractéristiques, concevoir des objets connectés et les relier à une plateforme IoT

**ECTS :** 3

**Nombre d'heures :** 40

**Enseignant :**



# SEMESTRE 2 : 30 crédits européens


UE 601


C


Compétences attendues


H

Techniques avancées de développement


**ECTS :** 10

110





### 602.2 : Programmation mobile 1

Maîtriser l'android SDK, faire communiquer des applications, services, capteurs

Plateforme : Android

https://programmation-mobile-1.netlify.app/

**ECTS :** 4

**Nombre d'heures :** 40h

**Enseignant :** Emmanuel Medina



### 602.3 : Programmation mobile 2


**ECTS :** 4


Maîtriser le SDK iOS, programmation Swift, architecture, services, capteurs

Plateforme : iOS
Enseignant :


40



UE 602


C


Compétences attendues


H

Projets tutorés


**ECTS :** 10


Projets de développement web/mobile donnant lieu à une production effective et réaliste, réalisés par groupe sous la supervision d'un enseignant. Production des documents de projet, présentation orale, démonstration.




Projet tutoré - découverte


**ECTS :** 5





75

Projet tutoré - réalisation


**ECTS :** 5





75



UE 603


C


Compétences attendues


S

Stage professionnel


**ECTS :** 10


Stage d'au moins 12 semaines en entreprise - production d'un rapport - présentation orale devant jury.


12 à 16



C : Coefficient ; H : Volume horaire ; S : Semaines
