---
layout: layouts/page.njk
date: Last Modified
title: CRUD
---

## Ajouter

```
HRAZ(Article)

SI Ouvre(FEN_Fiche_Article) ALORS
	TableAffiche(TABLE_Article,taCourantPremier)
FIN
```

## Modifier

```
SI TableSelect(TABLE_Article)=-1 ALORS RETOUR

Ouvre(FEN_Fiche_Article)

TableAffiche(TABLE_Article,taCourantBandeau)
```

## Supprimer

```
SI TableSelect(TABLE_Article) = -1 ALORS RETOUR

SI Dialogue("Êtes-vous sûr de vouloir supprimer l'enregistrement ?") = 1
	TableSupprime(TABLE_Article)
	TableAffiche(TABLE_Article, taCourantPremier)
FIN
```

## Enregistrer

```
EcranVersFichier()

HEnregistre(Article)

Ferme("",Vrai)
```
