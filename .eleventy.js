module.exports = function(config) {

  const cleancss = require("clean-css");
  config.addFilter("cleancss", function (code) {
    return new cleancss({}).minify(code).styles;
  });

  return { dir: { input: "src" } }
};
